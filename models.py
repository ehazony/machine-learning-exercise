from keras.wrappers.scikit_learn import KerasClassifier
from matplotlib import pyplot as plt
from sklearn.metrics import roc_auc_score, roc_curve
from sklearn.preprocessing import MinMaxScaler
from tensorflow.python.keras.engine.training_eager import test_on_batch
from sklearn.decomposition import PCA
from Dataloader import load_q1_data, TOP_WORDS, load_q2_data, load_fake_tweets
from sklearn.model_selection import train_test_split, KFold, cross_val_score
import numpy as np
from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Activation, Embedding, LSTM
import keras
from sklearn.decomposition import PCA

MAX_TWEET_LENGTH = 50
REAL_TWEET= 1
FAKE_TWEET =0





def baseline_model_q1():
    """
    :return: base LSTM model with sigmoid activation
    """
    embedding_vector_length = 32
    model = Sequential()
    model.add(Embedding(TOP_WORDS, embedding_vector_length, input_length=MAX_TWEET_LENGTH))
    model.add(LSTM(100))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    print(model.summary())
    return model

def baseline_model_q2():
    """
    :return: base LSTM model with softmax activation
    """
    embedding_vector_length = 32
    model = Sequential()
    model.add(Embedding(TOP_WORDS, embedding_vector_length, input_length=MAX_TWEET_LENGTH))
    model.add(LSTM(100))
    model.add(Dense(2, activation='softmax'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    print(model.summary())
    return model
def baseline_model_q2_for_fake_prediction():
    """
    the model will be updated with the baseline_model_q2 weights
    :return: base LSTM model with relu activation
    """
    embedding_vector_length = 32
    model = Sequential()
    model.add(Embedding(TOP_WORDS, embedding_vector_length, input_length=MAX_TWEET_LENGTH))
    model.add(LSTM(100))
    model.add(Dense(2, activation='relu'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    print(model.summary())
    return model


def load_base_model(num):
    """
    :param num: model qustion to load
    :return: loaded model
    """
    model = baseline_model_q1()
    model.load_weights('my_model_q'+str(num)+'_weights.h5')
    return model

def train_base_model_q1(X_train, y_train, X_test,y_test, save_weights = False):
    model = baseline_model_q1()
    history = model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=10, batch_size=16)
    if save_weights:
        model.save_weights('my_model_q1_weights.h5')
    return model, history

def train_base_model_q2(X_train, y_train , X_test, y_test,save_weights = False):
    model = baseline_model_q2()
    history = model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=20, batch_size=16)
    if save_weights:
        model.save_weights('my_model_q2_weights.h5')
    return model, history

def roc_plot(model, X_test, y_test):
    probs = model.predict_proba(X_test)
    # keep probabilities for the positive outcome only
    # calculate AUC
    print(model.evaluate(X_test,y_test))
    auc = roc_auc_score(y_test, probs)
    print('AUC: %.3f' % auc)
    # calculate roc curve
    fpr, tpr, thresholds = roc_curve(y_test, probs)
    # plot no skill
    plt.title('ROC Curve')
    plt.plot([0, 1], [0, 1], linestyle='--')
    # plot the roc curve for the model
    plt.plot(fpr, tpr, marker='.')
    # show the plot
    plt.show()

def vectorsie_tags(tags):
    """
    transforms tags from binary to 2 classes
    """
    return np.asarray([[0,1] if t ==1.0 else [1,0] for t in tags])

def plot_acc(history):
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.show()

def plot_loss(history):
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.show()

def is_fake(tweet):
    """
    question 2 additional functionality to determine that a given tweet is fake
    :return: 1 if real 0 if fake
    """
    model= baseline_model_q2_for_fake_prediction()
    model.load_weights('my_model_q2_weights.h5')
    prediction = model.predict(tweet)
    if prediction >= 1:
        return REAL_TWEET
    return FAKE_TWEET


if __name__ == "__main__":
    data = load_q1_data()

    #evening out the data set
    # trump = [list([a,b]) for a,b in data if b ==1.0]
    # hillary = [list([a,b]) for a,b in data if b ==0.0]
    # h_train, h_test = hillary[:len(trump)], hillary[len(trump):]
    # data = trump + h_train

    train, test = train_test_split(data, test_size=0.2)
    X_train, y_train = list(zip(*train))
    X_test, y_test = list(zip(*test))
    #chaning tags from binary to 2 classes for softmax
    # y_train = vectorsie_tags(np.array(y_train))
    # y_test = vectorsie_tags(np.array(y_test))
    y_train = np.array(y_train)
    y_test = np.array(y_test)
    #padding tweets so that they will be in the same length
    X_train = sequence.pad_sequences(X_train, maxlen=MAX_TWEET_LENGTH)
    X_test = sequence.pad_sequences(X_test, maxlen=MAX_TWEET_LENGTH)
    # training model
    model, history = train_base_model_q1(X_train, y_train,X_test,y_test, True)

    #acc matrix
    plot_acc(history)
    #loss matrex
    plot_loss(history)
    #roc metrix
    roc_plot(model, X_test, y_test)
