import csv

import nltk
from nltk.corpus import stopwords
import re
import numpy as np
from nltk.tokenize import TweetTokenizer
from Vocabulary import Vocabulary

TOP_WORDS = 500
VOCABULARY_FILE="analysis.vocab"

DATA_FILE_Q1="tweets_dataset_ex1.csv"
DATA_FILE_Q2="tweets_dataset_ex2.csv"
DATA_FILE_Q4="tweets_dataset_ex4.csv"

TRUMP_TWITER_NAME='realDonaldTrump'
HILLARY_TWITER_NAME='HillaryClinton'

def get_data(file_name):
    with open('data/'+file_name, encoding="utf8") as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        lines = [l for l in readCSV]
        donald = []
        hillary = []
        other = []
        for l in lines:
            if l[3]== 'True':
                continue
            if l[1].strip() == TRUMP_TWITER_NAME:
                donald.append(l[2])
            elif l[1] == HILLARY_TWITER_NAME:
                hillary.append(l[2])
            else:
                other.append((l[1], l[2]))
        return donald, hillary, other

def load_q1_data():
    trump, hillary, other = get_data(DATA_FILE_Q1)
    trump = trump[:len(hillary)]
    print("number of tweets for trump:" + str(len(trump)))
    print("number of tweets for hillary:" + str(len(hillary)))
    print("number of tweets not deffinde:" + str(len(other)))
    trump = preprocess_tweets(trump)
    hillary = preprocess_tweets(hillary)
    tweets_labeled = list(zip(trump, np.ones(len(trump))))
    tweets_labeled.extend(list(zip(hillary, np.zeros(len(hillary)))))
    vocab = Vocabulary(TOP_WORDS, VOCABULARY_FILE)
    tweets_text = [line[0] for line in tweets_labeled]
    vocab.PrepareVocabulary(tweets_text)
    reviews, labels = zip(*tweets_labeled)
    reviews_int = vocab.TransformSentencesToId(reviews)

    tweets_Labeled_Int = list(zip(reviews_int, labels))
    return tweets_Labeled_Int

def load_q2_data():
    trump, hillary, other = get_data(DATA_FILE_Q2)
    print("number of tweets for trump:" + str(len(trump)))
    print("number of tweets for hillary:" + str(len(hillary)))
    print("number of tweets not deffinde:" + str(len(other)))
    trump = preprocess_tweets(trump)
    hillary = preprocess_tweets(hillary)
    tweets_labeled = list(zip(trump, np.ones(len(trump))))
    tweets_labeled.extend(list(zip(hillary, np.zeros(len(hillary)))))
    vocab = Vocabulary(TOP_WORDS, VOCABULARY_FILE)
    tweets_text = [line[0] for line in tweets_labeled]
    vocab.PrepareVocabulary(tweets_text)
    reviews, labels = zip(*tweets_labeled)
    reviews_int = vocab.TransformSentencesToId(reviews)
    tweets_Labeled_Int = list(zip(reviews_int, labels))
    return tweets_Labeled_Int

def load_fake_tweets():
    with open('data/fake.csv', encoding="latin2") as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        lines = [i[0] for i in readCSV]
        vocab = Vocabulary(TOP_WORDS, VOCABULARY_FILE)
        return vocab.TransformSentencesToId(preprocess_tweets(lines))

REPLACE_NO_SPACE = re.compile("(\.)|(\;)|(\:)|(\!)|(\')|(\?)|(\,)|(\")|(\()|(\))|(\[)|(\])")
REPLACE_WITH_SPACE = re.compile("(<br\s*/><br\s*/>)|(\-)|(\/)")


def preprocess_tweets(tweets):
    tknzr = TweetTokenizer()
    default_stop_words = nltk.corpus.stopwords.words('english')
    stopwords = set(default_stop_words)
    tweets = [tknzr.tokenize(sentence) for sentence in tweets]
    tweets = [RemoveStopWords(line, stopwords) for line in tweets]
    return tweets


def RemoveStopWords(line, stopwords):
    words = []
    for word in line:
        word = word.strip()
        if word not in stopwords and word != "" and word != "&":
            words.append(word)

    return " ".join(words)

load_q1_data()